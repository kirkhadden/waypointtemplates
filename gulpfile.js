// Grab our gulp packages
var gulp  = require('gulp'),
    //liquid = require("gulp-liquidjs"),
    //liquid = require("gulp-liquid"), 
    liquify = require("gulp-liquify"), 
    gutil = require('gulp-util'),
    less = require('gulp-less'),
    sourcemaps = require('gulp-sourcemaps'),
    LessAutoprefix = require('less-plugin-autoprefix'),
    minifycss = require('gulp-minify-css'),
    jshint = require('gulp-jshint'),
    stylish = require('jshint-stylish'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    plumber = require('gulp-plumber'),
    bower = require('gulp-bower'),
    through = require('through2');




    var autoprefix = new LessAutoprefix({ browsers: ['last 2 versions'] });


gulp.task('liquid', function() {

    // return gulp.src("./src/*.liquid")
    //     .pipe(liquid())
    //     .pipe(rename({extname: ".html"}))
    //     .pipe(gulp.dest("./dist"));
    var locals = {
        name: "Fred"
    };
    return gulp.src("./src/*.liquid")
        .pipe(liquify(locals))
        .pipe(rename({extname: ".html"}))
        .pipe(gulp.dest("./dist"));
});

gulp.task('less', function() {

    return gulp.src([
            "./Styles/theme.less",
            "./Styles/bootstrap.less"
            //"./Styles/test.less"
        ])
        .pipe(less({
            plugins: [autoprefix]
        }))
        .pipe(rename({extname: ".css"}))
        .pipe(gulp.dest("./dist/Assets/css")); 
});

gulp.task('js', function() {
    return gulp.src([
            "./Assets/*.js",
        ])
        .pipe(gulp.dest("./dist/Assets/js")); 
});


// Watch files for changes
gulp.task('watch', function() {

  // Watch .less files
  gulp.watch('./Styles/**/*.less', ['less']);

  // Watch site-js files
  //gulp.watch('./assets/js/scripts/*.js', ['site-js']);
  
  // Watch templates files
  gulp.watch('./src/**/*.liquid', ['liquid']); 

});


