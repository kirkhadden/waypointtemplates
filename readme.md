# Bootstrap Templates and Styles for Waypoint Rock Site

This repository is a set of simple liquid templates and Bootstrap LESS files for use in making Waypoint Church's Rock-powered external website match their main WordPress-powered website. We used markup we found on the site as it is right now, combined with LESS files from the Stark theme. 

This repository contains:
* [src] Simple liquid templates for:
-- Event Registration
-- Groups Finder
-- Login
-- My Account
-- Serve Finder
* [Styles] All of the Bootstrap LESS files required to compile the final CSS for the theme
* A gulpfile for compiling the LESS and liquid templates uing Gulp
* [dist] The compiled html, css, and other assets

### Markup
We've tried to make our markup changes as minimal as possible. The only significant changes are in the following files:
* **src/parts/header.liquid** -- _Make the header and menu match waypointchurch.com_ 
* **src/parts/page-header.liquid** -- _Add a image header, and move the page title here._
* **src/parts/footer.liquid** -- _Make the footer resemble the footer on waypointchurch.com_

Where there are other changes, we've marked it with a comment beginning with <!-- ArtSpeak ... -->. 


### LESS
We've limited our LESS changes to:
* _variables.less
* theme.less

##### _variables.less
* Added variables for Waypoint colors, and applied them to Bootstrap elements
* Set global radius to 0px
* Applied Waypoint fonts

##### theme.less
Most of our additions are here, appended to the end of the document. 


